<?php
	//$cs = Yii::app()->getClientScript();
	//$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
    //header + menu
   // $this->renderPartial($layoutPath.'header', 
     //                   array(  "layoutPath"=>$layoutPath , 
       //                         "page" => "admin") ); 
?>
<!-- start: PAGE CONTENT -->

<?php
	$cs = Yii::app()->getClientScript();
	$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
    //header + menu
    $this->renderPartial($layoutPath.'header', 
                        array(  "layoutPath"=>$layoutPath , 
                                "page" => "admin",
                                "useFilter"=>false, "useHeader"=>true) );
    //BUTTON ADD +> TODO BOUBOULE GET GENERIC VIEW OF ADD ELEMENT DASHBOARD
	if(isset(Yii::app()->session["custom"])
		&& isset(Yii::app()->session["custom"]["htmlConstruct"])
		&& isset(Yii::app()->session["custom"]["htmlConstruct"]["adminPanel"])
		&& isset(Yii::app()->session["custom"]["htmlConstruct"]["adminPanel"]["add"])){
		$addElement=array(
		    Person::COLLECTION => array(
		        "label"=>Yii::t("common","Invite someone"),"icon"=>Person::ICON,"addClass"=> "bg-yellow lbhp", "color"=>"yellow", "href"=>"#element.invite"
		    ),
		    Organization::COLLECTION => array(
		        "label"=>Yii::t("common","Organizations"),"icon"=>Organization::ICON,"formType"=>"organization","addClass"=> "bg-green", "color"=>"green","href"=>"javascript:;"
		    ),
		    Project::COLLECTION => array(
		        "label"=>Yii::t("common","Project"),"icon"=>Project::ICON,"formType"=>"project","addClass"=> "addBtnFoot_orga addBtnFoot_project bg-purple","href"=>"javascript:;"
		    ),
		    Event::COLLECTION => array(
		        "label"=>Yii::t("common","Event"),
		        "icon"=>Event::ICON,
		        "formType"=>"event",
		        "addClass"=> "addBtnAll bg-orange",
		        "href"=>"javascript:;"
		    ),
		    Classified::COLLECTION => array(
		        "label"=>Yii::t("common","Classified"),
		        "icon"=>Classified::ICON,
		        "formType"=>"classifieds",
		        "addClass"=> "addBtnFoot_orga addBtnFoot_project bg-azure",
		        "href"=>"javascript:;"
		    ),
		    Classified::TYPE_RESSOURCES => array(
		        "label"=>Yii::t("common","Ressource"),
		        "icon"=>Classified::ICON_RESSOURCES,
		        "formType"=>"ressources",
		        "addClass"=> "addBtnAll bg-vine",
		        "href"=>"javascript:;"
		    ),
		    Classified::TYPE_JOBS => array(
		        "label"=>Yii::t("common","Jobs"),
		        "icon"=>Classified::ICON_JOBS,
		        "formType"=>"jobs",
		        "addClass"=> "hideBtnFoot_person addBtnFoot_orga addBtnFoot_project bg-yellow-k",
		        "href"=>"javascript:;"
		    ),
		    Poi::COLLECTION => array(
		        "label"=>Yii::t("common","Point of interest"),
		        "icon"=>Poi::ICON,
		        "formType"=>"poi",
		        "addClass"=> "addBtnAll bg-green-k",
		        "href"=>"javascript:;"
		    ),
		    Proposal::COLLECTION => array(
		        "label"=>Yii::t("common","Survey"),
		        "icon"=>Proposal::ICON,
		        "formType"=>"proposal",
		        "addClass"=> "addBtnAll bg-turq",
		        "color"=>"purple",
		        "href"=>"javascript:;"
		    )
		);
		if(isset(Yii::app()->session["custom"]["add"])){
	        foreach($addElement as $key=>$v)
	            if(!@Yii::app()->session["custom"]["add"][$key] && (!@$v["type"] || !@Yii::app()->session["custom"]["add"][$v["type"]])) unset($addElement[$key]);
	    }  
	}
	//STRUCTURE MENU ADMIN 
	// [ ] TODO BOUBOULE :: MOVE IT IN PARAMS JSON AND CUSTOM IT IN CO2.PHP AND FINALLY IN COSTUM MODULE 
	$menuAdmin=array(
        "directory" => array(
            "label"=>Yii::t("admin","Directory"),
            "super"=>true,
            "class"=> "text-yellow",
            "id"=> "btn-directory",
            "href"=>"javascript:;",
            "icon"=>"user"
        ),
        "reference" => array(
            "label"=>Yii::t("admin","Reference"),
            "sourceKey"=>true,
            "init"=>["organizations", "events", "projects"],
            "class"=> "text-azure",
            "id"=> "btn-reference",
            "href"=>"javascript:;",
            "icon"=>"creative-commons"
        ),
        "converter" => array(
            "label"=>Yii::t("common","Converter"),
           	"class"=> "text-green",
            "id"=> "btn-importdata",
            "href"=>"javascript:;",
            "icon"=>"upload"
        ),
        "import" => array(
            "label"=>Yii::t("common","IMPORT DATA"),
           	"class"=> "letter-blue",
            "id"=> "btn-adddata",
            "href"=>"javascript:;",
            "icon"=>"plus"
        ),
        "mails" => array(
            "label"=>Yii::t("admin","Mails simulator"),
            "super"=>true,
            "class"=> "text-purple",
            "id"=> "btn-mailslist",
            "href"=>"javascript:;",
            "icon"=>"at"
        ),
        "log" => array(
            "label"=>Yii::t("admin","LOG"),
            "super"=>true,
            "class"=> "text-dark",
            "id"=> "btn-log",
            "href"=>"javascript:;",
            "icon"=>"list"
        ),
        "moderation" => array(
            "label"=>Yii::t("admin","MODERATION"),
            "class"=> "text-red",
            "id"=> "btn-moderate",
            "href"=>"javascript:;",
            "icon"=>"check"
        ),
        "statistic" => array(
            "label"=>Yii::t("admin","Statitics"),
            "class"=> "text-orange",
            "id"=> "btn-statistic",
            "href"=>"javascript:;",
            "icon"=>"bar-chart"
        ),
        "mailerror" => array(
            "label"=>Yii::t("admin","Mail error "),
            "super"=>true,
            "class"=> "text-yellow",
            "id"=> "btn-mailerror",
            "href"=>"javascript:;",
            "icon"=>"envelope"
        )
    );
?>
<!-- start: PAGE CONTENT -->
<style type="text/css">
	#content-view-admin, #goBackToHome{
		display: none;
	}
	#content-social{
		min-height:700px;
		background-color: white;
	}
	.addServices, .show-form-new-circuit{
		display:none;
	}
</style>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" id="content-social">

	<?php if(@Yii::app()->session["userIsAdmin"] || Yii::app()->session["userIsAdminPublic"] 
				|| (@Yii::app()->session["userId"] 
                    && @Yii::app()->session["custom"]
                    && @Yii::app()->session["custom"]["admins"]
                    && @Yii::app()->session["custom"]["admins"][Yii::app()->session["userId"]])){ 
		$authorizedAdmin=true;
		$title=(@Yii::app()->session["userIsAdmin"]) ? Yii::t("common","Administration portal") : Yii::t("common","Public administration portal");
		$logo = (@Yii::app()->session['custom']["logo"]) ? Yii::app()->session['custom']["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-min.png";
		?>
	<div class="col-md-12 col-sm-12 col-xs-12" id="navigationAdmin">
		<div class="col-md-12 col-sm-12 col-xs-12 text-center">
			<img src="<?php echo $logo ?>" 
	                     class="" height="100"><br/>
	         <h3><?php echo $title ?></h3>
   		</div> 
   		<?php 
	    //Filtering button add element if custom
	    if(isset($addElement) && !empty($addElement)){ ?>
			<div class="col-xs-12  col-sm-offset-1 col-md-offset-2 col-md-8 col-sm-10 padding-50 margin-top-50 links-main-menu" 
			 id="div-select-create">
			<div class="col-md-12 col-sm-12 col-xs-12 padding-15 shadow2 bg-white ">
		       
		       <h4 class="text-center margin-top-15" style="">
		       	<a class="btn btn-link pull-right text-dark" id="btn-close-select-create" style="margin-top:-10px;">
		       		<i class="fa fa-times-circle fa-2x"></i>
		       	</a>
		       	<br>
		       	<i class="fa fa-plus-circle"></i> <?php echo Yii::t("form","Add content as source admin") ?>
		       	<br>
		       	<small><?php echo Yii::t("form","What kind of content will you create ?") ?></small>
		       </h4>

		        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
		        <?php foreach($addElement as $key => $v){
		        	if(!@$v["typeAllow"] || in_array($type, $v["typeAllow"])){ ?>
		        		<button data-form-type="<?php echo @$v["formType"] ?>" 
		        			<?php if(@$v["formSubType"]){ ?>
		        			data-form-subtype="<?php echo $v["formSubType"] ?>" 
		        			<?php } ?>
		        			data-dismiss="modal"
			                class="btn btn-link btn-open-form col-xs-6 col-sm-6 col-md-4 col-lg-4 text-<?php echo @$v["color"] ?>">
			            	<h6><i class="fa <?php echo @$v["icon"] ?> fa-2x"></i><br> <?php echo $v["label"] ?></h6>
			            	<small><?php echo @$v["description"] ?></small>
			        	</button>
		        	<?php }
		        }
		        ?>
		    </div>
	    </div>
	    <?php } ?>
		<ul class="list-group text-left no-margin">
		<?php foreach(Yii::app()->session["paramsConfig"]["adminPanel"] as $key => $v){
			if((!isset($v["super"]) || empty($v["super"]) || Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) && $key!="add"){ 
				if(!isset($v["sourceKey"]) || (isset(Yii::app()->session["custom"]) && isset(Yii::app()->session["custom"]["sourceKey"]))){?>
				<li class="list-group-item col-md-4 col-sm-6 ">
					<a href="javascript:;" class="<?php echo $v["class"] ?>" id="<?php echo $v["id"] ?>" style="cursor:pointer;">
						<i class="fa fa-<?php echo $v["icon"] ?> fa-2x"></i>
						<?php echo Yii::t("admin", $v["label"]); ?>
					</a>
				</li>
			<?php } 
			}
		} ?>
		<?php /*if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) { ?>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a href="javascript:;" class=" text-yellow" id="btn-directory" style="cursor:pointer;">
					<i class="fa fa-user fa-2x"></i>
					<?php echo Yii::t("admin", "Directory"); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-green" id="btn-importdata" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-upload fa-2x"></i>
					<?php echo Yii::t("common", "Converter"); ?>
				</a>
			</li>
			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-green" id="btn-mailslist" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-upload fa-2x"></i>
					<?php echo Yii::t("common", "Mails"); ?>
				</a>
			</li>
			
			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="letter-blue" id="btn-adddata" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-plus fa-2x"></i>
					<?php echo Yii::t("common", "IMPORT DATA"); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-dark" id="btn-log" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-list fa-2x"></i>
					<?php echo Yii::t("admin", "LOG"); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-red" id="btn-moderate" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-check fa-2x"></i>
					<?php echo Yii::t("admin", "MODERATION"); ?>
				</a>
			</li>
			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-orange" id="btn-statistic" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-bar-chart fa-2x"></i>
					<?php echo Yii::t("admin", "STATISTICS"); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-yellow" id="btn-mailerror" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-envelope fa-2x"></i>
					<?php echo Yii::t("admin", "MAILERROR"); ?>
				</a>
			</li>
			
		<?php 	}else if( Role::isSourceAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) ) ){ ?>
			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="text-green" id="btn-importdata" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-upload fa-2x"></i>
					<?php echo Yii::t("common", "Converter"); ?>
				</a>
			</li>
			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="letter-blue" id="btn-adddata" style="cursor:pointer;" href="javascript:;">
					<i class="fa fa-plus fa-2x"></i>
					<?php echo Yii::t("common", "IMPORT DATA"); ?>
				</a>
			</li>

		<?php	} */ ?>
		</ul>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding" id="goBackToHome">
		<a href="javascript:;" class="col-md-12 col-sm-12 col-xs-12 padding-20 text-center bg-orange" id="btn-home" style="font-size:20px;"><i class="fa fa-home"></i> Back to administrator home</a>
	</div>
	<div id="content-view-admin" class="col-md-12 col-sm-12 col-xs-12 no-padding"></div>
	<?php }else{ ?>
	<div class="col-md-12 col-sm-12 col-xs-12 text-center margin-top-50">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGOS/CO2/logo-min.png" 
	                     class="" height="100"><br/>
	         <h3><?php echo Yii::t("common","Administration portal") ?></h3>
   		</div>
		<div class="col-md-10 col-sm-10 col-xs-10 alert-danger text-center margin-top-20"><strong><?php echo Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>
	<?php } ?>
</div>
<!-- end: PAGE CONTENT-->
<script type="text/javascript">
	//	initKInterface(); 
	var superAdmin="<?php echo @Yii::app()->session["userIsAdmin"] ?>";
	var sourceAdmin="<?php echo @Yii::app()->session["userIsAdminPublic"] ?>";
	var authorizedAdmin=<?php echo json_encode(@$authorizedAdmin) ?>;
	var edit=true;
	var hashUrlPage = "#admin";
	var subView="<?php echo @$_GET['view']; ?>";
	var dir="<?php echo @$_GET['dir']; ?>";
	var paramsAdmin= <?php echo json_encode(Yii::app()->session["paramsConfig"]["adminPanel"]) ?>;
	jQuery(document).ready(function() {
		//loadDetail(true);
		if(superAdmin == "" && sourceAdmin == "" && !authorizedAdmin){
			urlCtrl.loadByHash("");
			bootbox.dialog({message:'<div class="alert-danger text-center"><strong><?php echo Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>'});
		}
		bindAdminButtonMenu();
		initKInterface();
		getAdminSubview(subView, dir);
		//KScrollTo("#topPosKScroll");
	});
	//function goProAccount(){
	//	urlCtrl.loadByHash("#page.type.citoyens.id."+contextData.id+".view.pro");
	//}
	function getAdminSubview(sub, dir){ console.log("getProfilSubview", sub, dir);
		if(sub!=""){
			if(sub=="directory")
				loadDirectory();
			if(sub=="reference")
				loadReference();
			else if(sub=="log")
				loadLog();
			else if(sub=="importdata")
				loadImport();
			else if(sub=="moderate")
				loadModerate();
			else if(sub=="statistic")
				loadStatistic();
			else if(sub=="mailerror")
				loadMailerror();
			else if(sub=="adddata") 
				loadAdddata();
			else if(sub=="mailslist")
				loadMailslist();
			/*else if(sub=="backups")
				loadBackup();
			else if(sub=="bookings"){
				loadListPro();
			}*/
		} else
			loadIndex();
	}
	function bindAdminButtonMenu(){
		/*$(".nav-link").click(function(){
			$(".podDash .nav .nav-item").removeClass("active");
			$(this).parent().addClass("active");
		});*/

		$("#btn-home").click(function(){
			location.hash=hashUrlPage;
			loadIndex();
		});
		$("#btn-directory").click(function(){
			location.hash=hashUrlPage+".view.directory";
			loadDirectory();
		});
		$("#btn-reference").click(function(){
			location.hash=hashUrlPage+".view.reference";
			loadReference();
		});
		
		$("#btn-log").click(function(){
			location.hash=hashUrlPage+".view.log";
			loadLog();
		});
		$("#btn-importdata").click(function(){
			location.hash=hashUrlPage+".view.createfile";
			loadImport();
		});
		$("#btn-moderate").click(function(){
			location.hash=hashUrlPage+".view.moderate";
			loadModerate();
		});
		$("#btn-statistic").click(function(){
			location.hash=hashUrlPage+".view.statistic";
			loadStatistic();
		});
		$("#btn-mailerror").click(function(){
			location.hash=hashUrlPage+".view.mailerror";
			loadMailerror();
		});

		$("#btn-mailslist").click(function(){
			location.hash=hashUrlPage+".view.mailslist";
			loadMailslist();
		});

		
		$("#btn-adddata").click(function(){
			location.hash=hashUrlPage+".view.adddata";
			loadAdddata();
		});
		$(".btn-open-form").click(function(){
			dyFObj.openForm($(this).data("form-type"),"sub");
		});
	}
	function loadIndex(){
		initDashboard(true);
		//var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
		//ajaxPost('.content-view-dashboard', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");
	}
	function loadDirectory(){
		initDashboard();
		searchAdminType=["citoyens"];
		data={initType:searchAdminType};
		var url = "admin/directory";
		$("#goBackToHome").show(700);
		//showLoader('.content-view-dashboard');
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, data, function(){},"html");
	}
	function loadReference(){
		initDashboard();
		searchAdminType=(typeof paramsAdmin != "undefined" 
			&& typeof paramsAdmin["reference"] != "undefined"
			&& typeof paramsAdmin["reference"]["initType"] != "undefined") ? paramsAdmin["reference"]["initType"]: ["organizations", "events", "projects"];
		data={initType:searchAdminType};
		var url = "admin/reference";
		$("#goBackToHome").show(700);
		//showLoader('.content-view-dashboard');
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, data, function(){},"html");
	}
	function loadLog(){
		initDashboard();
		var url = "log/monitoring";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}
	function loadModerate(){
		initDashboard();
		var url = "admin/moderate/one";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}
	function loadAdddata(){
		initDashboard();
		var url = "adminpublic/adddata";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}
	function loadImport(){
		initDashboard();
		var url = "adminpublic/createfile";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}
	function loadMailerror(){
		initDashboard();
		var url = "admin/mailerrordashboard";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}

	function loadMailslist(){
		initDashboard();
		var url = "adminpublic/mailslist";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}


	function loadStatistic(){
		initDashboard();
		var url = "app/info/p/stats";
		//showLoader('.content-view-dashboard');
		$("#goBackToHome").show(700);
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

	}
	
	function showLoader(id){
		$(id).html("<center><i class='fa fa-spin fa-refresh margin-top-50 fa-2x'></i></center>");
	}
	function inintDescs() {
		return true;
	}
	function initDashboard(home){
		if(home){
			$("#goBackToHome, #content-view-admin").hide(700);
			$("#navigationAdmin").show(700);
			//$("#goBackToHome .addServices, #goBackToHome .show-form-new-circuit").hide(700);
		} else {
			$("#navigationAdmin").hide(700);
			$("#goBackToHome, #content-view-admin").show(700);
			showLoader('#content-view-admin');
		}
	}
	
	function descHtmlToMarkdown() {
		mylog.log("htmlToMarkdown");
	}
</script>
<!--<div class="col-lg-offset-1 col-lg-10 col-xs-12 no-padding" id="content-social" style="min-height:700px;">
	<div class="">
		<ul class="list-group text-left no-margin">
		<?php if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) { ?>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-yellow" style="cursor:pointer;" href="#admin.view.directory">
					<i class="fa fa-user fa-2x"></i>
					<?php echo Yii::t("admin", "DIRECTORY", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>
			
			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-purple" style="cursor:pointer;" href="#adminpublic.view.createfile">
					<i class="fa fa-upload fa-2x"></i>
					<?php echo Yii::t("admin", "IMPORT DATA", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>

			 <li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-red" style="cursor:pointer;" href="#admin.view.openagenda">
					<i class="fa fa-calendar fa-2x"></i>
					<?php echo Yii::t("admin", "OPEN AGENDA", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-red" style="cursor:pointer;" href="#admin.view.checkgeocodage">
					<i class="fa fa-map fa-2x"></i>
					<?php echo Yii::t("admin", "CHECK GEOCODAGE", null, Yii::app()->controller->module->id); ?>
				</a>
			</li> 

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-red" style="cursor:pointer;" href="#adminpublic.view.adddata">
					<i class="fa fa-plus fa-2x"></i>
					<?php echo Yii::t("admin", "ADD DATA", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-green" style="cursor:pointer;" href="#log.monitoring">
					<i class="fa fa-list fa-2x"></i>
					<?php echo Yii::t("admin", "LOG", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-green" style="cursor:pointer;" href="#admin.view.checkcities">
					<i class="fa fa-list fa-2x"></i>
					<?php echo Yii::t("admin", "CHECK CITIES", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 link-to-moderate">
				<a class="lbh text-orange" style="cursor:pointer;" href="#admin.view.moderate.one">
					<i class="fa fa-check fa-2x"></i>                 
					<?php echo Yii::t("admin", "MODERATION", null, Yii::app()->controller->module->id); ?>               
				</a>

			<li class="list-group-item col-md-4 col-sm-6 link-to-moderate">
				<a class="lbh text-orange" style="cursor:pointer;" href="#stat.chartglobal">
					<i class="fa fa-bar-chart fa-2x"></i>              
					<?php echo Yii::t("admin", "STATISTICS", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>

			<li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-yellow" style="cursor:pointer;" href="#admin.view.mailerrordashboard">
					<i class="fa fa-envelope fa-2x"></i>                
					<?php echo Yii::t("admin", "MAILERROR", null, Yii::app()->controller->module->id); ?>              
				</a>
			</li>
			 <li class="list-group-item col-md-4 col-sm-6 ">
				<a class="lbh text-yellow" style="cursor:pointer;" href="#admin.view.cities">
					<i class="fa fa-university fa-2x"></i>               
					<?php echo Yii::t("admin", "CITIES", null, Yii::app()->controller->module->id); ?>              
				</a>
			</li>
         
<?php 	}
		
		if( Role::isSourceAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) ) ){ ?>
			<li class="list-group-item text-red col-md-4 col-sm-6 ">
				<a class="lbh" style="cursor:pointer;" href="#stat.chart">
					<i class="fa fa-plus fa-2x"></i>
					<?php echo Yii::t("admin", "SOURCE ADMIN", null, Yii::app()->controller->module->id); ?>
				</a>
			</li>
<?php	} ?>
		</ul>
	</div>
</div>-->
<!-- end: PAGE CONTENT-->

<script type="text/javascript">

/*jQuery(document).ready(function() {
	//setTitle("Espace administrateur","cog");
	//Index.init();
	initKInterface();
});*/

</script>