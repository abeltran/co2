<?php

$type = Organization::COLLECTION;
$elt = PHDB::findOne($type,array("slug" => "laRaffinerie"));
$idElt = (String)$elt["_id"] ;
$links = Element::getAllLinks($elt["links"], $type, $idElt);
$icon = Element::getFaIcon($type) ? Element::getFaIcon($type) : "";
$iconColor = Element::getColorIcon($type) ? Element::getColorIcon($type) : "";
$edit = false;

?>

<style type="text/css">
  #bg-homepage{
  	width: 100%;
  }
</style>

<nav class="col-sm-2 padding-10" style="color : white; background-color:#d4d4d4;">
	<ul>
		<?php
		if(!empty($elt) && !empty($elt["links"])  && !empty($elt["links"]["projects"]) && !empty($links) ){

			echo "<a href='javascript:;' class='linkMenu' data-key='".$idElt."' data-col='".Organization::CONTROLLER."'><li>".$elt["name"]."</li></a>";
			foreach ($elt["links"]["projects"] as $keyP => $valP) {
				if($links[$keyP]){
					echo "<a href='javascript:;' class='linkMenu' data-key='".$keyP."' data-col='".Project::CONTROLLER."'><li>".$links[$keyP]["name"]."</li></a>";
					// if(!empty($links[$keyP]["links"]) && !empty($links[$keyP]["links"]["projects"])){
					// 	echo "<ul>";
					// 	foreach ($links[$keyP]["links"]["projects"] as $keySP => $valSP) {

					// 		if($links[$keyP]){
					// 			echo "<a href='javascript:;' class='linkP' data-key='".$keyP."'><li>".$links[$keyP]["name"]."</li></a>";
					// 		}
					// 	}
					// 	echo "</ul>";
					// }
				}
			}
		}
		?>
	</ul>
</nav>
<div class="col-xs-10 padding-10" >

	
	<section class="col-xs-offset-1 col-xs-10" id="social-header" 
		<?php if (!@$elt["profilBannereUrl"] || (@$elt["profilBannereUrl"] && empty($elt["profilBannereUrl"]))){ ?> 
			style=" background: url('<?php echo Yii::app()->theme->baseUrl. '/assets/img/background-onepage/connexion-lines.jpg';?>') center bottom;"
		<?php } ?>>

		<label id="nameHeader"><?php echo $elt["name"] ; ?></label>
	</section>
</div>


<script type="text/javascript">
var elt = <?php echo json_encode($elt); ?> ;
var links = <?php echo json_encode($links); ?> ;
jQuery(document).ready(function() {

	mylog.log("elt", elt);
	mylog.log("links", links);
	setTitle("La Raffinerie");
	
	 bindBtn();
});

function bindBtn(){
	$(".linkMenu").click(function(){
		var key  = $(this).data("key");
		var col  = $(this).data("col");
		mylog.log("links", links[key]);

		$.ajax({
			type: "POST",
			url: baseUrl+'/co2/element/get/id/'+key+'/type/'+col,
			data: {},
			dataType: "json",
			success: function(data){
				mylog.log("data",data);

				$("#nameHeader").html(data.map.name);
			}
		});
	});

	// $(".orign").click(function(){
	// 	var key  = $(this).data("key");
	// 	mylog.log("links", links[key]);
	// });
}


</script>

<!-- 
TODO

- [ ] Liste des truc a récuperer avec la raffinerie
	- [ ] Logo
	- [ ] les couleurs du sites
	- [ ] la police si il en veut une 

 -->
